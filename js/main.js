(function ($) {
  // toggle mobile menu
  $("#hamburger-icon").click(() => {
    $(".mobile-header-container").toggleClass("closed");
    if ($(".mobile-header-container").hasClass("closed")) {
      //   enableScroll();
    } else {
      //   disableScroll();
    }
  });
  $("#mobile-navigation a").click(() => {
    $(".mobile-header-container").toggleClass("closed");
  });

  // toggle main header
  if ($(".mobile-header-container").hasClass("closed")) {
    const body = $("body");
    const scrollUp = "scroll-up";
    const scrollDown = "scroll-down";
    let lastScroll = 0;

    $(window).scroll(() => {
      const currentScroll = window.pageYOffset;
      if (currentScroll <= 0) {
        body.removeClass(scrollUp);
        return;
      }

      if (currentScroll > lastScroll && !body.hasClass(scrollDown)) {
        // down
        body.removeClass(scrollUp);
        body.addClass(scrollDown);
      } else if (currentScroll < lastScroll && body.hasClass(scrollDown)) {
        // up
        body.removeClass(scrollDown);
        body.addClass(scrollUp);
      }
      lastScroll = currentScroll;
    });
  }

  // Work swiper
  if ($("#work-swiper .swiper-slide").length > 3) {
    slidesxcol = 2;
  } else {
    slidesxcol = 1;
  }

  workSwiperDefaultParams = {
    // slidesPerView: "auto",
    slidesPerColumn: slidesxcol,
    spaceBetween: 24,
    slidesPerColumnFill: "row",
    breakpoints: {
      //>= 768
      769: {
        slidesPerView: "auto",
      },
    },
    navigation: {
      nextEl: "#work-swiper .swiper-button-next",
      prevEl: "#work-swiper .swiper-button-prev",
    },
  };
  workSwiper = new Swiper("#work-swiper", workSwiperDefaultParams);
  console.log(workSwiper.params.slidesPerView);

  // Work swiper on filter
  $(".work-swiper-filter li").on("click", function () {
    var filter = $(this).html();
    filter = filter.replace(/\s+/g, "-").toLowerCase();
    var slidesxcol;

    $(".work-swiper-filter li").removeClass("active");
    $(this).addClass("active");

    if (filter == "all") {
      $("#work-swiper div[data-filter]")
        .removeClass("non-swiper-slide")
        .addClass("swiper-slide")
        .show();

      if ($("#work-swiper .swiper-slide").length > 3) {
        slidesxcol = 2;
      } else {
        slidesxcol = 1;
      }

      workSwiper.destroy();
      workSwiperDefaultParams.slidesPerColumn = slidesxcol;
      workSwiper = new Swiper("#work-swiper", workSwiperDefaultParams);
    } else {
      $("#work-swiper .swiper-slide")
        .not("[data-filter='" + filter + "']")
        .addClass("non-swiper-slide")
        .removeClass("swiper-slide")
        .hide();
      $("#work-swiper div[data-filter='" + filter + "']")
        .removeClass("non-swiper-slide")
        .addClass("swiper-slide")
        .attr("style", null)
        .show();

      if ($("#work-swiper .swiper-slide").length > 3) {
        slidesxcol = 2;
      } else {
        slidesxcol = 1;
      }
      workSwiper.destroy();
      workSwiperDefaultParams.slidesPerColumn = slidesxcol;
      workSwiper = new Swiper("#work-swiper", workSwiperDefaultParams);
    }
  });

  // Work - case studies swiper

  var cstudiesSwiper = new Swiper("#cstudies-swiper", {
    navigation: {
      nextEl: "#cstudies-swiper .swiper-button-next",
      prevEl: "#cstudies-swiper .swiper-button-prev",
    },
    spaceBetween: 60,
    slidesPerView: 1.5,
    centeredSlides: true,
    initialSlide: 1,
    loop: true,
    // breakpoints
    breakpoints: {
      768: {
        spaceBetween: 20,
        slidesPerView: 1.5,
        centeredSlides: true,
        initialSlide: 1,
        loop: true,
      },
      425: {
        spaceBetween: 0,
        slidesPerView: 1,
        centeredSlides: true,
        initialSlide: 1,
        loop: true,
      },
    },
    on: {
      init: function () {
        $("#cstudies-swiper-wrapper").height($("#cstudies-swiper").height());
      },
    },
  });
  console.log(cstudiesSwiper.params);
})(jQuery);
